
# S.

For stages preview: clone this repo and navigate to [/stage_previews/with_results/](https://bitbucket.org/SADAVA/icy0006-project/src/master/stage_previews/with_results/) folder.

| Project description |                                           |
| ------------------- | ----------------------------------------- |
| Author              | Aleksander Tkatšenko                      |
| Repository url      | [https://bitbucket.org/SADAVA/icy0006-project/](https://bitbucket.org/SADAVA/icy0006-project/) |
| Stages progress     | ![](https://progress-bar.dev/7/?width=200&scale=7&suffix=%2F7) |
| Course name         | ICY0006 Probability Theory and Statistics |
| Start of project    | 6th of December, 2020                     |
| Deadline            | 4th of January, 2021                      |

| Dataset description |                                           |
| ------------------- | ----------------------------------------- |
| Topic               | Personal Loan Modeling                    |
| URL                 | [https://www.kaggle.com/teertha/personal-loan-modeling](https://www.kaggle.com/teertha/personal-loan-modeling) |
| Download date       | 6th of December, 2020                     |
| SHA256 checksum     | aee15529f84e9f0df51558c43e3c27199db66ddf3ff96b4073a382056bd036e6 |
| License             | Public Domain                             |
| Rows count          | 5000                                      |
| Missing values      | None                                      |
| Collumns info       | Customer data: demographic information, relationship with the bank, accepted loan. |



### End of the document